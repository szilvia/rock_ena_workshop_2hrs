# Reproducible Open Coding Kit (ROCK) and Epistemic Network Analysis 2-hr workshop

Rendered version of the script: https://szilvia.gitlab.io/rock_ena_workshop_2hrs <br>
Posit Cloud project for the workshop: https://posit.cloud/content/7960633 <br>

During the first part of the workshop, we will be using the {rock} to prepare, code, and segment our qualitative data. After this, we will use the same package to generate a few analyses and visualizations, and then finally, the qualitative data table. The second part of the workshop will focus on using the ENA webtool to generate network graphs; we will explore various model parameterizations and their effects on model interpretation.

## Reproducible Open Coding Kit (ROCK)

The ROCK is a standard for working with qualitative data implemented in an R package {rock}. It helps researchers organize data sources, designate attributes to the providers of the data, code and segment narratives, as well as perform various analyses. The "scripts" directory contains an .rmd file that can be employed to run {rock} commands; this script does not exhaust all of {rock} functionality. For more information on the ROCK, see https://rock.science.

## Epistemic Network Analysis (ENA)

ENA is the flagship tool of Quantitative Ethnography (QE), an approach to unified methods. ENA generates quantitative models of coded and segmented qualitative data, displaying the co-occurrence of unique pairs of codes within designated segments of data. To access the ENA webtool, visit https://www.epistemicnetwork.org; to access open educational materials on QE, see the QE Hub at: https://www.qehub.org.


<br>
<br>
